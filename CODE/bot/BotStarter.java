// // Copyright 2016 theaigames.com (developers@theaigames.com)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package bot;
import java.util.ArrayList;
import java.util.Random;

/**
 * BotStarter class
 * 
 * Magic happens here. You should edit this file, or more specifically
 * the makeTurn() method to make your bot do more than random moves.
 * 
 * @author Jim van Eeden <jim@starapple.nl>
 */

public class BotStarter {

	/*We recursively make a move. We first randomly get a move from our list of moves in case none of
	 * the moves are any better than the other. Then we iterate through all our moves, getting the values.
	 * If we are at the end of our recursive calls then we simply get the best valued move. Otherwise we
	 * get the best move from the next recursive call (by switching off between our move and our opponent's)
	 * and check whether the difference between a move and the best next move is better than any move we've
	 * seen so far. We keep track of moves this way and overall return the best move that maximizes our
	 * score and minimizes our opponent's. */
	public Move recursiveMakeTurn(Field field, ArrayList<Move> moves, int ID, int depth) {
		if (moves.size() < 1) return new Move();						//If there are no moves, return an arbitrary one.
		Random random = new Random();
		Move bestCurrMove = new Move();
		Move randomMove = moves.get(random.nextInt(moves.size()));		//Get a random move from our list of moves.
		bestCurrMove.mX = randomMove.mX;
		bestCurrMove.mY = randomMove.mY;
		int otherID;
		if (ID == 1) otherID = 2;
		else otherID = 1;
		
		for (int i = 0; i < moves.size(); i++) {						//Iterate over all our current moves.
			Move curr = moves.get(i);
			curr.value = getValue(field, curr, ID);						//Get the value of the move.
			if (depth == 2 && curr.value == 25) return curr;			//If it is our actual move and we can win, do it.
			if (depth != 0) {											//If we must still recurse then we do so.
				Field newField = updateField(field, curr, ID);
				ArrayList<Move> newMoves = newField.getAvailableMoves();//We get all possible moves.
				Move bestFutureMove = recursiveMakeTurn(newField, newMoves, otherID, depth - 1);	//We get the best move from this recursive call.
				if (curr.value - bestFutureMove.value > bestCurrMove.value) {	//If the difference between our move and the next move is better than any so far, save it.
					bestCurrMove.value = curr.value - bestFutureMove.value;
					if (depth == 2) {									//If we are at the very top, then save the x and y coordinates of the move.
						bestCurrMove.mX = curr.mX;
						bestCurrMove.mY = curr.mY;
					}
				}
			}
			else {														//If we are at the very bottom of the recursion, simply compare values and store the best one.
				if (curr.value > bestCurrMove.value)
					bestCurrMove.value = curr.value;
			}
		}
		return bestCurrMove;											//Return the best move we've seen.
	}
	
    /*Makes a move. It first checks to see our moves. If we have an empty board we play in the middle. Otherwise
     * if we only have 1 move then we make it. Otherwise we recursively find the best move with recursiveMakeTurn. */
	public Move makeTurn(Field field, int ID) {
		ArrayList<Move> moves = field.getAvailableMoves();
		if (field.isEmpty())											//If empty board, make a random move.
			return new Move(4, 4);
		else if (moves.size() == 1)										//If only one valid move, do it.
			return moves.get(0);
		else return recursiveMakeTurn(field, moves, ID, 2);
	}
	
	/*Gets the point value of the current move. We rank it like this - if we put something down it is
	 * worth 1 point. Blocking something of theirs so they can't win the row is 2 points. Putting 2 in
	 * a row down is 4 points. Blocking theirs with 2 in a row is 6 points. Winning a macro board is 9
	 * points. Blocking their macroboard win with our own win is 12 points. Winning 2 macros in a row is
	 * 16 points. Blocking their 2 macroboards in a row is 20 points. Winning the game is 25 points. */
	public int getValue(Field field, Move move, int ID) {
		int x = move.mX;
		int y = move.mY;
		int otherID;
		if (ID == 1) otherID = 2;
		else otherID = 1;
		
		/*We get how many in a spots (and macroboards) both players have in a row currently. */
		int ourAligned = howManyAligned(x, y, field, ID);
		int ourAlignedMacro = howManyAlignedMacro(x, y, field, ID);
		int theirsAligned = howManyAligned(x, y, field, otherID);
		int theirsAlignedMacro = howManyAlignedMacro(x, y, field, otherID);
		
		if (ourAligned == 2) {
			if (ourAlignedMacro == 0) {
				if (theirsAlignedMacro == 0) return 9;
				else if (theirsAlignedMacro == 1) return 12;
				else return 20;
			}
			else if (ourAlignedMacro == 1) return 16;
			else return 25;
		}
		else if (ourAligned == 1) {
			if (theirsAligned == 0) return 4;
			else return 1;
		}
		else {
			if (theirsAligned == 0) return 1;
			else if (theirsAligned == 1) return 2;
			else if (theirsAlignedMacro == 1) return 12;
			else if (theirsAlignedMacro == 2) return 20;
			else return 6;
		}
	}
	
	/*Updates the field with the given move. It modifys the current field by filling in our individually
	 * taken box with our ID and modifying the corresponding macroboards based on whether we won our current
	 * board, whether the other player would get to play in any of the other available macroboards, etc.
	 */
	public Field updateField(Field field, Move move, int ID) {
		int x = move.mX;
		int y = move.mY;
		int[][] currBoard = field.getBoard();							//We get a copy of the current board.
		int[][] newBoard = new int[9][9];
		for (int i = 0; i < 9; i++)										//We copy over the new board, filling in the spot we filled in with our ID.
			for (int j = 0; j < 9; j++)
				newBoard[j][i] = currBoard[j][i];
		newBoard[x][y] = ID;
		int[][] currMacro = field.getMacroboard();						//We get a copy of the macroboard.
		int[][] newMacro = new int[3][3];
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				newMacro[j][i] = currMacro[j][i];
		newMacro[x / 3][y / 3] = 0;										//We by default make the macroboard inactive.
		if (howManyAligned(x, y, field, ID) == 2)						//If this macroboard has been won, mark it with our ID.
			newMacro[x / 3][y / 3] = ID;
		if ((int)(x / 3) == x % 3 && (int)(y / 3) == y % 3 && newMacro[x / 3][y / 3] != ID)
			newMacro[x / 3][y / 3] = -1;								//If we were already in the square that the next player fills in then we make it available (if we didn't win it).
		else if (newMacro[x % 3][y % 3] == 1 || newMacro[x % 3][y % 3] == 2){	//If their next square has already been won then we make all unwon squares available.
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					if (newMacro[j][i] == 0)							//If some square was previously unavailable, we make it available for the next player.
						newMacro[j][i] = -1;
		}
		else															//Otherwise we just copy everything over and make the corresponding square that they get to play in available.
			newMacro[x % 3][y % 3] = -1;
		
		/*Make a new Field object and update it with new board and macroboards. */
		Field newField = new Field();
		newField.setBoard(newBoard);
		newField.setMacro(newMacro);
		return newField;
	}
	
	/*Gets the maximum number of spots that we have aligned with this play - used to see if we should
	 * make this move or not. */
	public int howManyAligned(int x, int y, Field field, int ID) {
		int horizontal = numberAlignedHorizontal(x, y, field, ID);		//Gets the number of row alignments.
		int vertical = numberAlignedVertical(x, y, field, ID);			//Gets the number of column alignments.
		int best;
		if (horizontal > vertical)
			best = horizontal;
		else best = vertical;											//Stores the maximum.
		
		/*If this spot is in one of the diagonals, check to see the number of alignments in whichever
		 * diagonals we are in. Store the maximum of these if so. */
		if (x % 3 == y % 3) {
			int diagonal = numberAlignedDiagonalTopLeft(x, y, field, ID);
			if (diagonal > best)
				best = diagonal;
		}
		if ((x % 3 == 0 && y % 3 == 2) || (x % 3 == 1 && y % 3 == 1) || (x % 3 == 2 && y % 3 == 0)) {
			int diagonal = numberAlignedDiagonalBottomLeft(x, y, field, ID);
			if (diagonal > best)
				best = diagonal;
		}
		return best;
	}
	
	/*Counts how many spots we have claimed in the current row in the current box. */
	public int numberAlignedHorizontal(int x, int y, Field field, int ID) {
		int leftMost = (int)(x / 3) * 3;
		int num = 0;
		for (int i = leftMost; i < leftMost + 3; i++)	//Check if we have any aligned in the current row.
			if (field.getPlayerId(i, y) == ID)
				num++;
		return num;
	}
	
	/*Counts how many spots we have claimed in the current column in the current box. */
	public int numberAlignedVertical(int x, int y, Field field, int ID) {
		int topMost = (int)(y / 3) * 3;
		int num = 0;
		for (int i = topMost; i < topMost + 3; i++)		//Check if we have any aligned in the current row.
			if (field.getPlayerId(x, i) == ID)
				num++;
		return num;
	}
	
	/*Counts how many spots we have claimed in the \ diagonal in the current box. */
	public int numberAlignedDiagonalTopLeft(int x, int y, Field field, int ID) {
		int leftMost = (int)(x / 3) * 3;
		int topMost = (int)(y / 3) * 3;
		int num = 0;
		for (int i = 0; i < 3; i++)
			if (field.getPlayerId(leftMost + i, topMost + i) == ID)
				num++;
		return num;
	}
	
	/*Counts how many spots we have claimed in the / diagonal in the current box. */
	public int numberAlignedDiagonalBottomLeft(int x, int y, Field field, int ID) {
		int leftMost = (int)(x / 3) * 3;
		int topMost = (int)(y / 3) * 3;
		int num = 0;
		for (int i = 0; i < 3; i++)
			if (field.getPlayerId(leftMost + i, topMost + 2 - i) == ID)
				num++;
		return num;
	}
	
	/*Gets the maximum number of macro boards that we have aligned in this move's row, column, and possibly
	 * diagonals if it is within those diagonals. */
	public int howManyAlignedMacro(int x, int y, Field field, int ID) {
		int horizontal = numberAlignedHorizontalMacro(x, y, field, ID);		//Gets the number of row alignments.
		int vertical = numberAlignedVerticalMacro(x, y, field, ID);			//Gets the number of column alignments.
		int best;
		if (horizontal > vertical)
			best = horizontal;
		else best = vertical;												//Stores the maximum.
		
		/*If this spot is in one of the diagonals, check to see the number of alignments in whichever
		 * diagonals we are in. Store the maximum of these if so. */
		if ((int)(x / 3) == (int)(y / 3)) {
			int diagonal = numberAlignedDiagonalTopLeftMacro(field, ID);
			if (diagonal > best)
				best = diagonal;
		}
		if (((int)(x / 3) == 0 && (int)(y / 3) == 2) || ((int)(x / 3) == 1 && (int)(y / 3) == 1) || ((int)(x / 3) == 2 && (int)(y / 3) == 0)) {
			int diagonal = numberAlignedDiagonalBottomLeftMacro(field, ID);
			if (diagonal > best)
				best = diagonal;
		}
		return best;
	}
	
	/*Checks how many macro boards have been filled up by us horizontally. */
	public int numberAlignedHorizontalMacro(int x, int y, Field field, int ID) {
		int num = 0;
		for (int i = 0; i < 9; i += 3)										//Check if we have any aligned in the current row.
			if (field.getPlayerIdMacro(i, y) == ID)
				num++;
		return num;
	}
	
	/*Checks how many macro boards have been filled up by us vertically. */
	public int numberAlignedVerticalMacro(int x, int y, Field field, int ID) {
		int num = 0;
		for (int i = 0; i < 9; i += 3)										//Check if we have any aligned in the current row.
			if (field.getPlayerIdMacro(x, i) == ID)
				num++;
		return num;
	}
	
	/*Counts how many macro boards we have claimed in the \ diagonal. */
	public int numberAlignedDiagonalTopLeftMacro(Field field, int ID) {
		int num = 0;
		for (int i = 0; i < 9; i += 3)
			if (field.getPlayerIdMacro(i, i) == ID)
				num++;
		return num;
	}
	
	/*Counts how many macro boards we have claimed in the / diagonal. */
	public int numberAlignedDiagonalBottomLeftMacro(Field field, int ID) {
		int num = 0;
		for (int i = 0; i < 9; i += 3)
			if (field.getPlayerIdMacro(i, 6 - i) == ID)
				num++;
		return num;
	}
	
	public static void main(String[] args) {
		BotParser parser = new BotParser(new BotStarter());
		parser.run();
	}
}
